<?php

use App\Models\Mark;
use Illuminate\Database\Seeder;

class MarksTableSeeder extends Seeder
{
    /** @var int */
    private const MARK_COUNT = 200;

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(Mark::class, self::MARK_COUNT)->create();
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrigger extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('
            CREATE TRIGGER Add_Post_Mark AFTER INSERT ON `marks` FOR EACH ROW
            BEGIN
                UPDATE
                    `posts`
                SET
                    `mark_count` = `mark_count` + 1,
                    `mark_sum`   = `mark_sum` + NEW.`mark`
                WHERE `id` = NEW.`post_id`;
            END
        ');

        DB::unprepared('
            CREATE TRIGGER Remove_Post_Mark AFTER DELETE ON `marks` FOR EACH ROW
            BEGIN
                UPDATE
                    `posts`
                SET
                    `mark_count` = `mark_count` - 1,
                    `mark_sum`   = `mark_sum` - OLD.`mark`
                WHERE `id` = OLD.`post_id`;
            END
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP TRIGGER `Add_Post_Mark`');
        DB::unprepared('DROP TRIGGER `Remove_Post_Mark`');
    }
}

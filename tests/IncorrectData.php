<?php

namespace Tests;

interface IncorrectData
{
    const POST_INCORRECT_DATA = [
        [
            'body'   => 'Test post',
            'author' => 'Test user',
            'ip'     => '127.0.0.1',
        ],
        [
            'title'  => 'Test',
            'author' => 'Test user',
            'ip'     => '127.0.0.1',
        ],
        [
            'title'  => 'Test',
            'body'   => 'Test post',
            'ip'     => '127.0.0.1',
        ],
        [
            'title'  => 'Test',
            'body'   => 'Test post',
            'author' => 'Test user',
        ],
        [
            'title'  => 'Test',
            'body'   => 'Test post',
            'author' => 'Test user',
            'ip'     => '127.0.0.0.1',
        ],
        [
            'title'  => 'Test',
            'body'   => 'Test post',
            'author' => 'Test user',
            'ip'     => '256.0.0.1',
        ],
    ];

    const MARK_INCORRECT_DATA = [
        [
            'mark' => 0,
        ],
        [
            'mark' => 7,
        ],
        [
            'mark' => 3.5,
        ],
        [
            'mark' => 'abc',
        ],
    ];

    const LIST_OF_POST_INCORRECT_DATA = [
        [
            'limit' => -1,
        ],
        [
            'mark' => 3.5,
        ],
        [
            'mark' => 'abc',
        ],
    ];
}

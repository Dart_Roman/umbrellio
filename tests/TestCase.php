<?php

namespace Tests;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase implements CorrectData, IncorrectData, ResourceStructures
{
    use CreatesApplication, RefreshDatabase;
}

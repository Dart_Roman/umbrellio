<?php

namespace Tests\Feature;

use App\Models\Mark;
use App\Models\Post;
use App\Models\User;
use Tests\TestCase;

class RatePostTest extends TestCase
{
    private const NUMBER_OF_MARKS = 10;

    /** @test */
    public function can_rate_post_with_correct_data()
    {
        $user = factory(User::class)->create();
        $post = factory(Post::class)->create();

        $this->post(route('mark.store', ['post' => $post]), self::MARK_CORRECT_DATA)
            ->assertStatus(200)
            ->assertJsonStructure(self::MARK_STRUCTURE);

        $this->assertDatabaseHas('marks', [
            'mark'    => self::MARK_CORRECT_DATA['mark'],
            'post_id' => $post->id,
        ]);
    }

    /** @test */
    public function can_not_rate_post_with_incorrect_data()
    {
        $user = factory(User::class)->create();
        $post = factory(Post::class)->create();

        foreach (self::MARK_INCORRECT_DATA as $marktData) {
            $this->post(route('mark.store', ['post' => $post]), $marktData)
                ->assertStatus(422);

            $this->assertDatabaseMissing('marks', [
                'mark'    => $marktData['mark'],
                'post_id' => $post->id,
            ]);
        }
    }

    /** @test */
    public function recalculate_work_correctly()
    {
        $user = factory(User::class)->create();
        $post = factory(Post::class)->create();

        for ($i = 1; $i <= self::NUMBER_OF_MARKS; $i++) {
            $this->post(route('mark.store', ['post' => $post]), [
                'mark' => mt_rand(1, 5)
            ]);
        }

        $sum = Mark::all()->sum('mark');

        $this->assertEquals(Post::find($post->id)->mark, $sum / self::NUMBER_OF_MARKS);
    }
}

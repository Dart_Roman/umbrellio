<?php

namespace Tests\Feature;

use Tests\TestCase;

class CreatePostTest extends TestCase
{
    /** @test */
    public function can_create_post_with_correct_data()
    {
        $this->post(route('post.store'), self::POST_CORRECT_DATA)
            ->assertStatus(201)
            ->assertJsonStructure(self::POST_STRUCTURE);

        $this->assertDatabaseHas('posts', [
            'title'      => self::POST_CORRECT_DATA['title'],
            'body'       => self::POST_CORRECT_DATA['body'],
            'ip'         => self::POST_CORRECT_DATA['ip'],
            'user_id'    => 1,
            'mark_count' => 0,
            'mark_sum'   => 0,
        ]);
    }

    /** @test */
    public function not_create_new_same_author()
    {
        $this->post(route('post.store'), self::POST_CORRECT_DATA)
            ->assertStatus(201)
            ->assertJsonStructure(self::POST_STRUCTURE);

        $this->post(route('post.store'), self::POST_CORRECT_DATA_2)
            ->assertStatus(201)
            ->assertJsonStructure(self::POST_STRUCTURE);

        $this->assertDatabaseHas('posts', [
            'title'      => self::POST_CORRECT_DATA_2['title'],
            'body'       => self::POST_CORRECT_DATA_2['body'],
            'ip'         => self::POST_CORRECT_DATA_2['ip'],
            'user_id'    => 2,
            'mark_count' => 0,
            'mark_sum'   => 0,
        ]);
    }

    /** @test */
    public function can_not_create_post_with_incorrect_data()
    {
        foreach(self::POST_INCORRECT_DATA as $postData) {
            $this->post(route('post.store'), $postData)
                ->assertStatus(422);

            $this->assertDatabaseMissing('posts', [
                'title'      => $postData['title'] ?? '',
                'body'       => $postData['body'] ?? '',
                'ip'         => $postData['ip'] ?? '',
                'user_id'    => 1,
                'mark_count' => 0,
                'mark_sum'   => 0,
            ]);
        }
    }
}

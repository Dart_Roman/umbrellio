<?php

namespace Tests\Feature;

use App\Models\Mark;
use App\Models\Post;
use App\Models\User;
use Tests\TestCase;

class GetListOfIpTest extends TestCase
{
    /** @var int */
    private const NUMBER_OF_POSTS = 5;

    /** @var int */
    private const NUMBER_OF_AUTHORS = 2;

    /** @test */
    public function can_get_list_of_ip_with_correct_data()
    {
        factory(User::class, self::NUMBER_OF_AUTHORS)->create();
        factory(Post::class, self::NUMBER_OF_POSTS)->create([
            'ip' => '127.0.0.1',
        ]);

        $response = $this->get(route('ip.list'))->assertStatus(200)
            ->assertJsonStructure(self::LIST_OF_IP_STRUCTURE)
            ->assertJsonCount(1, 'data');
    }

    /** @test */
    public function can_get_empty_list_of_ip_with_correct_data()
    {
        $user = factory(User::class, self::NUMBER_OF_AUTHORS)->create();
        $post = factory(Post::class, self::NUMBER_OF_POSTS)->create();

        $response = $this->get(route('ip.list'))->assertStatus(200)
            ->assertJsonStructure(self::LIST_OF_IP_STRUCTURE)
            ->assertJsonCount(0, 'data');
    }
}

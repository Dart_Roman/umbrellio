<?php

namespace Tests\Feature;

use App\Models\Mark;
use App\Models\Post;
use App\Models\User;
use Tests\TestCase;

class GetListOfPostsTest extends TestCase
{
    private const NUMBER_OF_POSTS = 10;

    /** @test */
    public function can_get_list_of_posts_with_correct_data()
    {
        $user = factory(User::class)->create();
        $post = factory(Post::class, self::NUMBER_OF_POSTS)->create();

        $response = $this->get(
            route('post.list') . '?' . http_build_query(self::LIST_OF_POST_CORRECT_DATA)
        )->assertStatus(200)
            ->assertJsonStructure(self::LIST_OF_POST_STRUCTURE)
            ->assertJsonCount(self::LIST_OF_POST_CORRECT_DATA['limit'], 'data');
    }

    /** @test */
    public function can_not_get_list_of_posts_with_incorrect_data()
    {
        $user = factory(User::class)->create();
        $post = factory(Post::class, self::NUMBER_OF_POSTS)->create();

        foreach(self::LIST_OF_POST_INCORRECT_DATA as $postData) {
            $this->post(route('post.list') . '?' . http_build_query($postData))
                ->assertStatus(422);
        }
    }
}

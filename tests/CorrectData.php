<?php

namespace Tests;

interface CorrectData
{
    const POST_CORRECT_DATA = [
        'title'  => 'Test',
        'body'   => 'Test post',
        'author' => 'Test user',
        'ip'     => '127.0.0.1',
    ];

    const POST_CORRECT_DATA_2 = [
        'title'  => 'Test post 2',
        'body'   => 'Test post for checking work with author',
        'author' => 'Test user',
        'ip'     => '127.0.0.1',
    ];

    const MARK_CORRECT_DATA = [
        'mark' => 4,
    ];

    const LIST_OF_POST_CORRECT_DATA = [
        'limit' => 5,
    ];
}

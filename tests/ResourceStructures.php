<?php

namespace Tests;

interface ResourceStructures
{
    const POST_STRUCTURE = [
        'data' => [
            'id',
            'title',
            'body',
            'author_id',
            'ip',
            'mark',
        ]
    ];

    const LIST_OF_POST_STRUCTURE = [
        'data' => [
            '*' => [
                'id',
                'title',
                'body',
                'author_id',
                'ip',
                'mark',
            ]
        ]
    ];

    const MARK_STRUCTURE = [
        'data' => [
            'mark',
        ]
    ];

    const LIST_OF_IP_STRUCTURE = [
        'data' => [
            '*' => [
                'ip',
                'authors',
            ]
        ]
    ];
}

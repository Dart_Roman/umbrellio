<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('/post', 'PostController@store')->name('post.store');
Route::get('/post', 'PostController@list')->name('post.list');
Route::post('/mark/{post}', 'MarkController@store')->name('mark.store');
Route::get('/ip', 'IpController@list')->name('ip.list');

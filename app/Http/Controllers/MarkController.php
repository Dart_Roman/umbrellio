<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\MarkCreateRequest;
use App\Http\Resources\MarkResource;
use App\Models\Mark;
use App\Models\Post;
use Illuminate\Http\Request;

class MarkController extends Controller
{
    public function store(MarkCreateRequest $request, Post $post)
    {
        Mark::create([
            'post_id' => $post->id,
            'mark'    => $request->mark,
        ]);

        return new MarkResource($post);
    }
}

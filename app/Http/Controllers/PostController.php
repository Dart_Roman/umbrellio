<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\PostCreateRequest;
use App\Http\Requests\PostGetListRequest;
use App\Http\Resources\PostResource;
use App\Models\Post;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PostController extends Controller
{
    public function store(PostCreateRequest $request)
    {
        $user = User::firstOrCreate(['login' => $request->author]);
        $post = Post::create([
            'title'   => $request->title,
            'body'    => $request->body,
            'ip'      => $request->ip,
            'user_id' => $user->id,
        ]);

        return new PostResource($post);
    }

    public function list(PostGetListRequest $request)
    {
        $posts = Post::orderBy(DB::raw('mark_sum / mark_count'), 'desc')
            ->limit($request->limit)
            ->get();

        return PostResource::collection($posts);
    }
}

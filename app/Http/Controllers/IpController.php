<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Resources\IpResource;
use App\Models\Post;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class IpController extends Controller
{
    public function list()
    {
        $ips = Post::select('ip', DB::raw("group_concat(DISTINCT user_id) AS authors"))
            ->groupBy('ip')
            ->having(DB::raw('COUNT(DISTINCT user_id)'), '>', 1)
            ->get();

        return IpResource::collection($ips);
    }
}

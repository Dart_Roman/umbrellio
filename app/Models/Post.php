<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'body',
        'ip',
        'user_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'mark_count',
        'mark_sum',
    ];

    public function getMarkAttribute() {
        if ($this->mark_count) {
            return $this->mark_sum / $this->mark_count;
        }

        return 0;
    }
}
